function creaGrafica(_entidad,_municipio) {
    entidad=_entidad;
    municipio=_municipio;
    nombreMuni=" ";
    nombreEnti=" ";
    //////////////////////////////////////////////
    var urlM="http://localhost/api/public/api/entidades/"+entidad+"/municipios/"+municipio;
    var xhrM = new XMLHttpRequest();
    xhrM.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status === 200) {
          var documento_xml_muni = xhrM.responseXML;
             values=new Array();
    nombre_mun=documento_xml_muni.getElementsByTagName('nombre');
    nombreMuni=nombreMuni+nombre_mun[0].innerHTML; 
        } else if (this.status === 403) {
            archivo_a_descargar.value = "Error: 403 Forbidden.";
        } else if (this.status === 404) {
            archivo_a_descargar.value = "Error: 404 Not Found.";
        } else {
            archivo_a_descargar.value = "Error: " + xhrM.status;
        }
      }
    };
   var urlE="http://localhost/api/public/api/entidades/"+entidad;
    var xhrE = new XMLHttpRequest();
    xhrE.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status === 200) {
          var documento_xml_enti = xhrE.responseXML;
             values=new Array();
    nombre_en=documento_xml_enti.getElementsByTagName('nombre');
    nombreEnti=nombreEnti+nombre_en[0].innerHTML; 
        } else if (this.status === 403) {
            archivo_a_descargar.value = "Error: 403 Forbidden.";
        } else if (this.status === 404) {
            archivo_a_descargar.value = "Error: 404 Not Found.";
        } else {
            archivo_a_descargar.value = "Error: " + xhrE.status;
        }
      }
    };

    ///////////////////////////////////////////
datoIndicador=new Array();
 datoNombre=new Array();
 datoTotal=new Array();
 periodos=new Array();
 /////////////////////////////////////////////////////////////////
 var url="http://localhost/api/public/api/entidades/"+entidad+"/municipios/"+municipio+"/indicadores";
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status === 200) {
          var documento_xml = xhr.responseXML;
             values=new Array();
    valores=documento_xml.getElementsByTagName('indicador');
    for (var i = 0; i < valores.length; i++) {
            dato=0;
            indicador=valores[i].children[0].innerHTML;
            descripcion=valores[i].children[1].innerHTML;
            periodos[i]=valores[i].children[2];
            for(var j=0;j<periodos[i].children.length;j++){
                dato=parseFloat(periodos[i].children[j].children[1].innerHTML)+dato;
            }
            datoNombre[i]=indicador+" "+descripcion;
            datoIndicador[i]=dato;
        }
    for (var k = 0; k < datoIndicador.length; k++) {
            datoTotal[k]={
                name:datoNombre[k],
                y:datoIndicador[k]
            };
        }
        //////////////////////////////////////////////////////
            $('#container').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Relacion total de accidentes por tipo en la entidad '+nombreEnti+' Municipio '+nombreMuni
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Porcentaje',
            data: datoTotal
        }]
    });
        //////////////////////////////////////////////////////
        } else if (this.status === 403) {
            archivo_a_descargar.value = "Error: 403 Forbidden.";
        } else if (this.status === 404) {
            archivo_a_descargar.value = "Error: 404 Not Found.";
        } else {
            archivo_a_descargar.value = "Error: " + xhr.status;
        }
      }
    };
    xhrE.open('GET', urlE);
    xhrE.setRequestHeader("Accept", "application/xml");
    xhrE.send();
    xhrM.open('GET', urlM);
    xhrM.setRequestHeader("Accept", "application/xml");
    xhrM.send();
    xhr.open('GET', url);
    xhr.setRequestHeader("Accept", "application/xml");
    xhr.send();
    //////////////////////////////////////////
///////////////////////////////////////////

}


